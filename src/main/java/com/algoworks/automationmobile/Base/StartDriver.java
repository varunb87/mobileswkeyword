package com.algoworks.automationmobile.Base;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;
import org.testng.ITestContext;
import com.algoworks.automationmobile.Utility.TestNGParameters;
import com.google.common.base.Throwables;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class StartDriver
{
	private static ThreadLocal<AppiumDriver<MobileElement>> threadDriver = new ThreadLocal<>();
	private static ThreadLocal<String> testName = new ThreadLocal<>();
	private static Logger logger = LogManager.getLogger(StartDriver.class);

	public void initializeDriver(final ITestContext testContext, CreateCapabilities create)
	{
		testName.set(testContext.getName()); // Lets set the test name in the threadlocal string
		TestNGParameters parameters = new TestNGParameters(testContext);
		try
		{
			AppiumDriver<MobileElement> driver = create.createDriver(testContext);
			setDriver(driver);
		}
		catch (WebDriverException e)
		{
			if (Throwables.getStackTraceAsString(e).contains("Connection refused: connect"))
			{
				logger.fatal("Failure! Please ensure that Appium server is running & the emulator/simulator/real device is connected & functioning properly! ");
				Assert.fail("Failure! Please ensure that Appium server is running & the emulator/simulator/real device is connected & functioning properly! ", e);
			}
			else if (Throwables.getStackTraceAsString(e).contains("was not in the list of connected devices"))
			{
				logger.fatal("Device " + parameters.getDevicename() + " having UDID " + parameters.getUDID() + " is not connected! Please connect & initialize teh device properly before running the test.");
				Assert.fail("Device " + parameters.getDevicename() + " having UDID " + parameters.getUDID() +" is not connected! Please connect & initialize teh device properly before running the test.");
			}
			else
			{
				logger.fatal(e);
				Assert.fail(Throwables.getStackTraceAsString(e));
			}
		}
		catch (Exception e)
		{
			logger.fatal(e);
			Assert.fail(Throwables.getStackTraceAsString(e));
		}
	}

	private void setDriver(AppiumDriver<MobileElement> driver)
	{
		threadDriver.set(driver);
	}

	public static AppiumDriver<MobileElement> getDriver()
	{
		return threadDriver.get();
	}

	/*
	 * This method will be used by Error reporting class to retrieve the Test name that had the error
	 */
	public static String getTestName()
	{
		return (testName.get());
	}
}
