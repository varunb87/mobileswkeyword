package com.algoworks.automationmobile.Base;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.algoworks.automationmobile.Reporting.*;
import com.algoworks.automationmobile.Utility.ArrayActions;
import com.algoworks.automationmobile.Utility.BuildPaths;
import com.algoworks.automationmobile.Utility.FindSpreadsheet;
import com.algoworks.automationmobile.Utility.TestNGParameters;
import com.google.common.base.Throwables;

public class MainInitiator
{
	private String excelfilePath;
	private static ThreadLocal<ExtentReporting> extentThread = new ThreadLocal<>();
	private static ThreadLocal<String> threadTestname = new ThreadLocal<>();
	private SoftAssert softAssert;
	private String testname = null;
	private Object[][] testData = null;
	private CreateCapabilities create;
	private static Logger logger = LogManager.getLogger(MainInitiator.class);

	@BeforeTest
	public void setup(final ITestContext testContext)
	{
		String excelFileName = "";
		try
		{
			TestNGParameters parameters = new TestNGParameters(testContext);
			create = new CreateCapabilities();
			testname = testContext.getCurrentXmlTest().getName();
			excelFileName = parameters.getExcelFilename()
					.trim(); /* Adding trim to remove silly mistakes made by the QA when specifying file names */

			// Now define absolute paths
			excelfilePath = BuildPaths.getResourcePath(parameters) + excelFileName;
			String regression = testContext.getCurrentXmlTest().getParameter("testType");

			FindSpreadsheet find = new FindSpreadsheet();
			testData = find.getArray(excelfilePath, regression);
			System.out.println();
			System.out.println();
			logger.info(excelFileName + " read successfully.");
		}
		catch (FileNotFoundException e)
		{
			logger.fatal(e);
			Assert.fail(Throwables.getStackTraceAsString(e));
		}
		catch (IOException e)
		{
			if (e.getMessage().contains("The process cannot access the file because it is being used by another process"))
			{
				logger.fatal(excelFileName + " is being used by another process. Please close the file first.");
			}
			Assert.fail(Throwables.getStackTraceAsString(e));
		}
		catch (Exception e)
		{
			logger.fatal(Throwables.getStackTraceAsString(e));
			// e.printStackTrace();
			Assert.fail(Throwables.getStackTraceAsString(e));
		}
	}

	@DataProvider(name = "regression")
	public Object[][] createDP()
	{
		/*
		 * This is being done because dataprovider reads from row 0 but our row 0 contains only the headings
		 * of columns. We want to exclude that
		 */
		List<List<Object>> temp = new ArrayList<>();
		Object[][] dp = null;
		if (testData != null && StartDriver.getDriver() != null)
		{
			int index = -1;
			for (int i = 1; i < testData.length; i++)
			{
				if (!testData[i][0].toString().toLowerCase().contains("round") && testData[i][2].toString().equalsIgnoreCase("y"))
				{
					++index;
					temp.add(index, new ArrayList<>());
					for (int j = 0; j < testData[i].length; j++)
					{
						temp.get(index).add(testData[i][j]);
					}
				}
			}
			ArrayActions aa = new ArrayActions();
			dp = aa.getArrayfromList(temp);
		}
		else
		{
			dp = new Object[0][0];
		}
		return dp;
	}

	@BeforeClass
	public void startTest(final ITestContext testContext)
	{
		TestNGParameters parameters = new TestNGParameters(testContext);
		StartDriver start = new StartDriver();
		try
		{
			/* Create report name & send the desired report file path to Extent Reports for logging */
			String reportName = testname + "_" + parameters.getDevicename() + "_" + System.getProperty("buildNumber") + ".html";

			/* Initialize the Android or iOS driver here */
			start.initializeDriver(testContext, create);
			logger.info("Connection with " + parameters.getDevicename() + " was successful.");
			
			/* Once driver is up & running we can create the folder for reports & screenshots */
			String reportfilePath = BuildPaths.getBuildPath() + reportName;
			ExtentReporting reporting = new ExtentReporting();
			reporting.createReports(reportfilePath, parameters.getPlatform());
			extentThread.set(reporting);
		}
		catch (Exception e)
		{
			logger.fatal("Unknown exception found in BeforeClass", e);
			Assert.fail("Unknown exception found in BeforeClass", e);
		}
	}

	@Test(dataProvider = "regression")
	public void runTest(String testCase, String testDescription, String execution, final ITestContext testContext)
	{
		TestNGParameters parameters = new TestNGParameters(testContext);
		TestRunner itd = new TestRunner();
		softAssert = new SoftAssert();
		threadTestname.set(testname);
		System.out.println();
		logger.info("Currently executing " + "\"" + testCase + "\"");
		testDescription = testDescription + " on " + parameters.getDevicename() + " having UDID = " + parameters.getUDID();
		getExtentReportingObject().createTest(testCase, testDescription);
		try
		{
			itd.initTestCase(excelfilePath, testCase, testContext, softAssert);
			softAssert.assertAll();
		}
		catch (EncryptedDocumentException e)
		{
			logger.fatal("EncryptedDocumentException found in @Test", e);
			Assert.fail("EncryptedDocumentException found in @Test", e);
		}
	}

	@AfterTest
	public void stoptest()
	{
		if (StartDriver.getDriver() != null)
		{
			System.out.println();
			logger.info("Mobile Automation will now stop.");
			StartDriver.getDriver().quit();
		}
	}

	@AfterClass
	public void runAfterTest()
	{
		if (getExtentReportingObject() != null)
			getExtentReportingObject().endTests();
	}

	public static ExtentReporting getExtentReportingObject()
	{
		return extentThread.get();
	}

	public static String getTestCaseName()
	{
		return threadTestname.get();
	}
}