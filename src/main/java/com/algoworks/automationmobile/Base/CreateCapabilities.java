package com.algoworks.automationmobile.Base;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import com.algoworks.automationmobile.Utility.BuildPaths;
import com.algoworks.automationmobile.Utility.TestNGParameters;
import com.algoworks.automationmobile.Utility.Utils;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileCapabilityType;

public class CreateCapabilities
{
	private AppiumDriver<MobileElement> driver;
	private static Logger logger = LogManager.getLogger(CreateCapabilities.class);

	public AppiumDriver<MobileElement> createDriver(final ITestContext testContext) throws MalformedURLException
	{
		DesiredCapabilities capabilities = new DesiredCapabilities();
		String iOSAppPath = null;
		TestNGParameters parameters = new TestNGParameters(testContext);
		if (("android").equalsIgnoreCase(parameters.getPlatform()))
		{
			String androidAppAbsolutePath = BuildPaths.getAppPath(parameters) + parameters.getAppname().trim();
			capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, Platform.ANDROID);
			capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");
			capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.ANDROID_UIAUTOMATOR2);
			capabilities.setCapability(MobileCapabilityType.UDID, parameters.getUDID());
			capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, parameters.getDevicename());
			capabilities.setCapability(MobileCapabilityType.CLEAR_SYSTEM_FILES, true);
			capabilities.setCapability(MobileCapabilityType.NO_RESET, false);
			capabilities.setCapability("autoGrantPermissions", true);
			//capabilities.setCapability(MobileCapabilityType.FULL_RESET, true);
			capabilities.setCapability(MobileCapabilityType.APP, androidAppAbsolutePath);
			if (Utils.isNotEmptyAndBlank(parameters.getApppackage()))
			{
				capabilities.setCapability("appPackage", parameters.getApppackage());
			}
			if (Utils.isNotEmptyAndBlank(parameters.getAppActivity()))
			{
				capabilities.setCapability("appActivity", parameters.getAppActivity());
			}
			driver = new AndroidDriver<>(new URL("http://" + parameters.getServerIPAddress() + ":" + parameters.getPortnumber() + "/wd/hub"), capabilities);
		}

		else if ("ios".equalsIgnoreCase(parameters.getPlatform()))
		{
			iOSAppPath = BuildPaths.getAppPath(parameters) + parameters.getAppname();
			capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, Platform.IOS);
			capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, parameters.getPlatformVersion());
			capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);
			capabilities.setCapability(MobileCapabilityType.CLEAR_SYSTEM_FILES, true);
			capabilities.setCapability(MobileCapabilityType.FULL_RESET, true);
			/*
			 * In case of IOS emulator there is no need of UDID. For actual devices only!
			 */
			if (Utils.isNotEmptyAndBlank(parameters.getUDID()))
			{
				capabilities.setCapability(MobileCapabilityType.UDID, parameters.getUDID());
			}
			capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, parameters.getDevicename());
			capabilities.setCapability(MobileCapabilityType.APP, new File(iOSAppPath).getAbsolutePath());
			logger.info("All device capabilities have been gathered. Initializing Appium driver...");
			driver = new IOSDriver<>(new URL("http://" + parameters.getServerIPAddress() + ":" + parameters.getPortnumber() + "/wd/hub"), capabilities);
		}
		return driver;
	}
}
