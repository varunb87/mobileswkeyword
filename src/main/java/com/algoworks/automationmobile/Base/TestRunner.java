package com.algoworks.automationmobile.Base;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.ElementNotSelectableException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.InvalidSelectorException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.asserts.SoftAssert;
import com.algoworks.automationmobile.Errors.ErrorReporting;
import com.algoworks.automationmobile.KeywordAction.PerformAction;
import com.algoworks.automationmobile.Utility.FindSpreadsheet;
import com.algoworks.automationmobile.Utility.Utils;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class TestRunner
{
	private PerformAction performAction;
	public void initTestCase(String excelFilePath, String testCase, ITestContext testContext, SoftAssert softAssert)
	{
		String failMsg = null;
		AppiumDriver<MobileElement> driver = StartDriver.getDriver();
		FindSpreadsheet find = new FindSpreadsheet();
		String action;
		String element; 
		String category;
		String value;
		int child;
		boolean isList = false;
		Throwable t = null;
		try
		{
			Object[][] objectarray = find.getArray(excelFilePath, testCase);
			String[][] testSteps = find.getStringArrayfromObject(objectarray);
			performAction = new PerformAction(testContext);
			ErrorReporting errorReport = new ErrorReporting(driver);
			List<String> dataErrors = new ArrayList<>();
			for (int column = 6; !testSteps[0][column].trim().isEmpty(); column++)
			{
				for (int row = 1; row < testSteps.length; row++)
				{
					value = testSteps[row][column];
					if (value.equalsIgnoreCase("skip") || value.contains("skip"))
						continue;
					else
					{
						action = testSteps[row][1];
						element = testSteps[row][2];
						child = Integer.parseInt((testSteps[row][3]));
						isList = Boolean.parseBoolean((testSteps[row][4]));
						category = testSteps[row][5];
						try
						{
							performAction.perform(action, element, child, isList, category, value);
						}
						catch (NullPointerException e)
						{
							failMsg = "NullPointerException encountered!";
							t = e;
							dataErrors.add(action);
							dataErrors.add(element);
							dataErrors.add(value);
							dataErrors.add(Integer.toString(row)); // add row number
							errorReport.logErrors(t, testCase, dataErrors, failMsg, softAssert);
						}
						catch (InvalidSelectorException e)
						{
							failMsg = "InvalidSelectorException! " + element + " was not found. Please recheck your locator & the locator type in the Excel sheet & then re-run the test.";
							t = e;
							dataErrors.add(action);
							dataErrors.add(element);
							dataErrors.add(value);
							dataErrors.add(Integer.toString(row)); // add row number
							errorReport.logErrors(t, testCase, dataErrors, failMsg, softAssert);
						}
						catch (NoSuchElementException e)
						{
							failMsg = "NoSuchElementException! " + element + " was not found. Please recheck your locator in the Excel sheet & re-run the test.";
							t = e;
							dataErrors.add(action);
							dataErrors.add(element);
							dataErrors.add(value);
							dataErrors.add(Integer.toString(row)); // add row number
							errorReport.logErrors(t, testCase, dataErrors, failMsg, softAssert);
						}
						catch (ElementNotVisibleException e)
						{
							failMsg = performAction.getFailMessage();
							if(Utils.isEmptyOrBlank(failMsg))
							{
								failMsg = "ElementNotVisibleException! " + element + " was not visible on the page. Please recheck your locator in the Excel sheet & re-run the test.";
							}
							t = e;
							dataErrors.add(action);
							dataErrors.add(element);
							dataErrors.add(value);
							dataErrors.add(Integer.toString(row)); // add row number
							errorReport.logErrors(t, testCase, dataErrors, failMsg, softAssert);
						}
						catch (ElementNotSelectableException e)
						{
							failMsg = "ElementNotSelectableException! " + element
									+ " cannot be selected. Please recheck your locator in the Excel sheet & re-run the test.";
							t = e;
							dataErrors.add(action);
							dataErrors.add(element);
							dataErrors.add(value);
							dataErrors.add(Integer.toString(row)); // add row number
							errorReport.logErrors(t, testCase, dataErrors, failMsg, softAssert);
						}
						catch (TimeoutException e)
						{
							failMsg = "TimeoutException! Operation timed out & " + element + " was not found!";
							t = e;
							dataErrors.add(action);
							dataErrors.add(element);
							dataErrors.add(value);
							dataErrors.add(Integer.toString(row)); // add row number
							errorReport.logErrors(t, testCase, dataErrors, failMsg, softAssert);
						}
						catch (AssertionError | Exception e)
						{
							failMsg = performAction.getFailMessage(); // this will contain some assertion failure message.
							t = e;
							dataErrors.add(action);
							dataErrors.add(element);
							dataErrors.add(value);
							dataErrors.add(Integer.toString(row)); // add row number
							errorReport.logErrors(t, testCase, dataErrors, failMsg, softAssert);
						}
					}
				}
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			throw new SkipException("Skipping " + testCase + " because file was not found.");
		}
		catch (ArrayIndexOutOfBoundsException | NullPointerException | EncryptedDocumentException
				| InvalidFormatException e)
		{
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
