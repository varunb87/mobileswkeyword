package com.algoworks.automationmobile.Reporting;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.openqa.selenium.Platform;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReporting
{
	private ExtentReports extentReporter;
	private ExtentTest extentTest;
	private static ThreadLocal<ExtentTest> extentThreadTest = new ThreadLocal<>();

	public void createReports(String reportname, String platform) throws UnknownHostException
	{
		// initialize the HtmlReporter
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(reportname);
		extentReporter = new ExtentReports();

		// attach only HtmlReporter
		extentReporter.attachReporter(htmlReporter);
		htmlReporter.setAppendExisting(true);
		extentReporter.setSystemInfo("Operating System", System.getProperty("os.name"));
		if ("android".equalsIgnoreCase(platform))
		{
			extentReporter.setSystemInfo("Platform", Platform.ANDROID.toString());
		}
		else
		{
			extentReporter.setSystemInfo("Platform", Platform.IOS.toString());
		}
		//extentReporter.setSystemInfo("Platform", platform);
		extentReporter.setSystemInfo("Computer Name", InetAddress.getLocalHost().getHostName());
		extentReporter.setSystemInfo("Java Version",
				System.getProperty("java.runtime.name") + " " + System.getProperty("java.runtime.version"));
	}

	public void createTest(String currentTestName, String testDescription)
	{
		ExtentTest extentTests = extentReporter.createTest(currentTestName, testDescription);
		extentThreadTest.set(extentTests);
	}

	public ExtentTest getExtentTest()
	{
		return extentThreadTest.get();
	}

	public void displayWarning(String warnMessage)
	{
		extentTest = getExtentTest();
		extentTest.log(Status.WARNING, warnMessage);
	}

	public void endTests()
	{
		extentReporter.flush();
	}

	public void passTest(String passMessage)
	{
		extentTest = getExtentTest();
		extentTest.log(Status.PASS, passMessage);
	}

	public void failTest(String failMessage, String finalScreenshot) throws IOException
	{
		extentTest = getExtentTest();
		extentTest.log(Status.FAIL, failMessage).addScreenCaptureFromPath(finalScreenshot);
	}

	public void errorTest(String failMessage, String finalScreenshot) throws IOException
	{
		extentTest = getExtentTest();
		extentTest.log(Status.ERROR, failMessage).addScreenCaptureFromPath(finalScreenshot);
	}

	public void writeInfo(String info)
	{
		extentTest = getExtentTest();
		extentTest.log(Status.INFO, info);
	}

	public void failReport(String failMessage)
	{
		extentTest = getExtentTest();
		extentTest.log(Status.FAIL, failMessage);
		extentReporter.flush();
	}
}
