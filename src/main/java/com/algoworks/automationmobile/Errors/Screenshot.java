package com.algoworks.automationmobile.Errors;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.algoworks.automationmobile.Utility.BuildPaths;

import io.appium.java_client.AppiumDriver;

public class Screenshot
{
	private AppiumDriver<?> driver = null;

	public Screenshot(AppiumDriver<?> driver)
	{
		this.driver = driver;
	}

	public String takeScreenshot(String testName, String sheetName, int rowNumber) throws IOException
	{
		File scrFile = driver.getScreenshotAs(OutputType.FILE);
		String screenshotPath = null;
		screenshotPath = System.getProperty("user.dir") + BuildPaths.getSeparator() + "src" + BuildPaths.getSeparator() + "resources" + BuildPaths.getSeparator();
		String screenshotFilename = testName + sheetName + "_" + rowNumber + ".png";
		String finalScreenshotPath = screenshotPath + screenshotFilename;
		FileUtils.copyFile(scrFile, new File(finalScreenshotPath));

		finalScreenshotPath = "file://" + finalScreenshotPath;
		return finalScreenshotPath;
	}

}
