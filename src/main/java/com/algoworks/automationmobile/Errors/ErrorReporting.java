package com.algoworks.automationmobile.Errors;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.InvalidSelectorException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import com.algoworks.automationmobile.Base.MainInitiator;
import com.algoworks.automationmobile.Base.StartDriver;
import com.algoworks.automationmobile.Reporting.ExtentReporting;
import com.google.common.base.Throwables;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class ErrorReporting
{
	private AppiumDriver<MobileElement> driver;
	private static Logger logger = LogManager.getLogger(ErrorReporting.class);
	
	public ErrorReporting(AppiumDriver<MobileElement> driver)
	{
		this.driver = driver;
	}

	public void logErrors(Throwable e, String sheetName, List<String> dataErrors, String failMsg, SoftAssert softAssert) throws IOException
	{
		String testName = StartDriver.getTestName();
		Screenshot screenshot = new Screenshot(driver);
		System.out.println();
		logger.info("Test name having error = " + testName);
		logger.info("Here are the errors: ");
		logger.info("Sheet name: " + sheetName);
		Iterator<String> itr = dataErrors.iterator();
		logger.info("Action = " + itr.next());
		logger.info("Element = " + itr.next());
		
		String value = itr.next();
		if (!StringUtils.isBlank(value) && !StringUtils.isEmpty(value))
			logger.info("Value = " + value);
		int rowNumber = Integer.parseInt(itr.next()) + 1; //this is done to match row number in Excel
		logger.info("Excel sheet row number that has error = " + rowNumber);
		
		//Clear the errors list
		dataErrors.clear();
		ExtentReporting testing = MainInitiator.getExtentReportingObject();
		String screenshotPath = null;
		try
		{
			screenshotPath = screenshot.takeScreenshot(testName, sheetName, rowNumber);
		}
		catch (IOException io)
		{
			logger.debug("IOException found while taking screenshot", io);
		}
		logger.info("Screenshot saved at = " + screenshotPath);
		
		if (e instanceof InvalidSelectorException || e instanceof TimeoutException || e instanceof ElementNotVisibleException || e instanceof NoSuchElementException)
		{
			testing.errorTest(failMsg, screenshotPath);  //log the error in the HTML report
			logger.error(failMsg);
			System.out.println(e.getMessage());
			softAssert.fail(Throwables.getStackTraceAsString(e));
		}
		else if (e instanceof NullPointerException)
		{
			testing.errorTest(failMsg, screenshotPath);  //log the error in the HTML report
			logger.error(failMsg);
			Assert.fail(Throwables.getStackTraceAsString(e));
		}
		else if (e instanceof AssertionError)
		{
			logger.error(failMsg);
			testing.failTest(failMsg, screenshotPath);
			System.out.println();
			softAssert.fail(Throwables.getStackTraceAsString(e));
		}
		else
		{
			testing.errorTest(e.getMessage(), screenshotPath);
			logger.fatal("Test ended abruptly!", e);
			Assert.fail(Throwables.getStackTraceAsString(e));
		}
	}
}
