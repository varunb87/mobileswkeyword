package com.algoworks.automationmobile.Utility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

public class FindSpreadsheet
{
	public Object[][] getArray(String excelFilePath, String testCaseSheet) throws InvalidFormatException, IOException
	{
		Object[][] testStep = null;
		ReadExcel read = new ReadExcel();
		testStep = read.returnArray(excelFilePath, testCaseSheet);

		/* Now remove null rows from the array */
		List<List<Object>> temp = new ArrayList<>();
		int listindex = 0;
		for (int i = 0; i < testStep.length; i++)
		{
			if (testStep[i] != null)
			{
				temp.add(listindex, new ArrayList<>());
				for (int j = 0; j < testStep[i].length; j++)
				{
					if (testStep[i][j] != null)
					{
						temp.get(i).add(testStep[i][j]);
					}
				}
				++listindex;
			}
		}

		/* Now convert 2d arraylist to 2d object array */
		ArrayActions aa = new ArrayActions();
		return aa.getArrayfromList(temp);
	}

	public String[][] getStringArrayfromObject(Object[][] array)
	{
		String[][] finalarray = new String[array.length][];
		for (int i = 0; i < array.length; i++)
		{
			finalarray[i] = Arrays.stream(array[i]).toArray(String[]::new);
		}
		return finalarray;
	}
}