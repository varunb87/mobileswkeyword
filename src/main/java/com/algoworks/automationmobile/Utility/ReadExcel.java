package com.algoworks.automationmobile.Utility;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.testng.Assert;

public class ReadExcel
{
	@SuppressWarnings("deprecation")
	public Object[][] returnArray(String excelFileName, String sheetname) throws InvalidFormatException, IOException
	{
		Object[][] arr = null;
		int rownum = 0;
		int col;
		Workbook wb = null;
		File excelFileToRead = new File(excelFileName);
		Sheet sheet = null;
		wb = WorkbookFactory.create(excelFileToRead);
		sheet = wb.getSheet(sheetname);
		wb.close();
		rownum = sheet.getPhysicalNumberOfRows();

		if (rownum == 0 || rownum == 1)
		{
			Assert.fail("Please ensure that Excelsheet has atleast 2 rows: header row + data row");
		}
		// this is to find max column number only

		arr = new String[rownum][];
		for (int r = 0; r < rownum; r++)
		{
			Row row = sheet.getRow(r); // if row not defined then return
										// null
			if (row == null) // Skip the row if its entirely null
				continue;
			col = row.getPhysicalNumberOfCells();
			arr[r] = new String[col];
			for (int c = 0; c < col; c++)
			{
				Cell cell = row.getCell(c);
				{
					if (cell != null)
						switch (cell.getCellType())
						{
							case XSSFCell.CELL_TYPE_NUMERIC:
							{
								arr[r][c] = getNumericCellValue(cell);
								break;
							}

							case XSSFCell.CELL_TYPE_STRING:
							{
								arr[r][c] = cell.getStringCellValue();
								break;
							}

							case XSSFCell.CELL_TYPE_BLANK:
							{
								arr[r][c] = " ";
								break;
							}

							case XSSFCell.CELL_TYPE_FORMULA:
							{
								switch (cell.getCachedFormulaResultType())
								{
									case Cell.CELL_TYPE_STRING:
										arr[r][c] = cell.getRichStringCellValue().getString();
										break;
									case Cell.CELL_TYPE_NUMERIC:
										arr[r][c] = getNumericCellValue(cell);
										break;
								}
								break;
							}
						}
				}
			}
		}
		return arr;
	}

	private static String getNumericCellValue(Cell cell)
	{
		double value = cell.getNumericCellValue();
		NumberFormat f = NumberFormat.getInstance();
		f.setGroupingUsed(false);
		return f.format(value);
	}
}
