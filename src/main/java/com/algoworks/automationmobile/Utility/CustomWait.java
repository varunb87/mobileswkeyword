package com.algoworks.automationmobile.Utility;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class CustomWait
{
	private WebDriverWait wait;
	private AppiumDriver<MobileElement> driver;

	public CustomWait(AppiumDriver<MobileElement> driver)
	{
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 30);
	}

	public void waitforElements(By locator, String listsize) throws InterruptedException
	{
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
		int attempts = 0;
		boolean found = false;
		while (attempts <= 30 && !found)
		{
			try
			{
				if (("small".equalsIgnoreCase(listsize) && driver.findElements(locator).size() == 1) || ("large".equalsIgnoreCase(listsize) && driver.findElements(locator).size() > 1))
				{
					found = true;
				}
			}
			catch (WebDriverException e)
			{
				++attempts;
			}
		}
	}

	public void waitforElement(By locator) throws InterruptedException
	{
		int attempts = 0;
		boolean found = false;
		while (attempts <= 60 && !found)
		{
			Thread.sleep(100);
			try
			{
				if (driver.findElement(locator).isDisplayed())
				{
					found = true;
				}
			}
			catch (WebDriverException e)
			{
				++attempts;
			}
			catch (NullPointerException e)
			{
				++attempts;
			}
		}

		/* Wait<AppiumDriver> wait = new FluentWait<AppiumDriver>(driver).withTimeout(45, TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		wait.until(new Function<AppiumDriver, MobileElement>()
		{
			public MobileElement apply(AppiumDriver driver)
			{
				return (MobileElement) driver.findElement(locator);
			}
		});*/
	}
}
