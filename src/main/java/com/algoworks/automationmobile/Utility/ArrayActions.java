package com.algoworks.automationmobile.Utility;

import java.util.List;

public class ArrayActions
{
	public Object[][] getArrayfromList(List<List<Object>> temp)
	{
		Object[][] array = new Object[temp.size()][];
		for (int i = 0; i < temp.size(); i++)
		{
			List<Object> dummy = temp.get(i);
			array[i] = dummy.toArray(new Object[dummy.size()]);
		}
		return array;
	}
}
