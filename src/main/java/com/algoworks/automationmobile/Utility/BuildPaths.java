package com.algoworks.automationmobile.Utility;

import java.io.File;
import org.apache.commons.lang3.SystemUtils;

public class BuildPaths
{
	private static final String USERDIRECTORY = System.getProperty("user.dir");

	private BuildPaths()
	{
	}

	/* XLSX FILE READ LOGIC */
	public static String getResourcePath(TestNGParameters parameters)
	{
		String path = null;

		/*
		 * If the excel file path is mentioned in the XML then read the path from there. This means the path
		 * will need to be hardcoded in XML.
		 */
		if (Utils.isNotEmptyAndBlank(parameters.getExcelFileDirectory()))
		{
			path = parameters.getExcelFileDirectory();
			if (!path.endsWith(getSeparator()) && SystemUtils.IS_OS_WINDOWS)
			{
				path = path + getSeparator();
			}
		}
		else
		{
			path = USERDIRECTORY + getSeparator() + "src" + getSeparator() + "main" + getSeparator() + "resources" + getSeparator();
		}
		return path.trim(); /* Adding trim to remove silly mistakes made by QA like adding extra space */
	}

	/* APP PATH READ LOGIC */
	/* This is to correct any user errors made while entering the APK directory */
	public static String getAppPath(TestNGParameters parameters)
	{
		String path = parameters.getAppDirectory().trim();
		if (Utils.isNotEmptyAndBlank(path) && !(SystemUtils.IS_OS_WINDOWS))
		{
			if (!path.endsWith("/"))
			{
				path = path + getSeparator();
			}
			if (!path.startsWith("/"))
			{
				path = getSeparator() + path;
			}
		}
		else if (Utils.isNotEmptyAndBlank(path) && (SystemUtils.IS_OS_WINDOWS))
		{
			if (!path.endsWith("\\"))
			{
				path = path + getSeparator();
			}
		}
		else
		{
			path = USERDIRECTORY + getSeparator();
		}
		return path;
	}

	public static String getSeparator()
	{
		return System.getProperty("file.separator");
	}

	public static String getBuildPath()
	{
		String buildPath = null;
		buildPath = USERDIRECTORY + getSeparator() + System.getProperty("buildTag") + getSeparator();
		File file = new File(buildPath);
		if (!file.exists())
		{
			file.mkdirs();
		}
		return buildPath;
	}
}
