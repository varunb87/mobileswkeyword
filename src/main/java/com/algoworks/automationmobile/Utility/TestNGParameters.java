package com.algoworks.automationmobile.Utility;

import org.testng.ITestContext;

public class TestNGParameters
{
	private ITestContext testContext;
	public TestNGParameters(final ITestContext testContext)
	{
		this.testContext = testContext;
	}
	
	public String getPlatform()
	{
		return testContext.getCurrentXmlTest().getParameter("platform");
	}
	
	public String getPlatformVersion()
	{
		return testContext.getCurrentXmlTest().getParameter("platformVersion");
	}
	
	public String getUDID()
	{
		return testContext.getCurrentXmlTest().getParameter("udid");
	}
	
	public String getDevicename()
	{
		return testContext.getCurrentXmlTest().getParameter("deviceName");
	}
	
	public String getServerIPAddress()
	{
		return testContext.getCurrentXmlTest().getParameter("server");
	}
	
	public String getPortnumber()
	{
		return testContext.getCurrentXmlTest().getParameter("port");
	}
	
	public String getAppname()
	{
		return testContext.getCurrentXmlTest().getParameter("appfile");
	}
	
	public String getApppackage()
	{
		return testContext.getCurrentXmlTest().getParameter("package");
	}
	
	public String getAppActivity()
	{
		return testContext.getCurrentXmlTest().getParameter("activity");
	}
	
	public String getExcelFileDirectory()
	{
		return testContext.getCurrentXmlTest().getParameter("excelFilepath");
	}
	
	public String getExcelFilename()
	{
		return testContext.getCurrentXmlTest().getParameter("sheetName");
	}
	
	public String getAppDirectory()
	{
		return testContext.getCurrentXmlTest().getParameter("appFilepath");
	}

}
