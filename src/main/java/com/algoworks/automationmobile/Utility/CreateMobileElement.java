package com.algoworks.automationmobile.Utility;

import org.openqa.selenium.By;

public class CreateMobileElement
{
	public By getLocator(String element, String category)
	{
		By locator = null;
		switch (category.toLowerCase()) // in case someone enters caps in excel then convert all to lower
		{
			case "class":
				locator = By.className(element);
				break;

			case "class name":
				locator = By.className(element);
				break;

			case "id":
				locator = getIDlocator(element);
				break;

			case "name":
				locator = By.xpath("//*[@name='" + element + "']");
				break;

			case "resource id":
				locator = getIDlocator(element);
				break;

			case "tagname":
				locator = By.tagName(element);
				break;

			case "text":
				locator = By.xpath("//*[@text='" + element + "']");
				break;
				
			case "xpath":
				locator = By.xpath(element);
				break;

			default:
				break;
		}
		return locator;
	}

	private By getIDlocator(String element)
	{
		return By.id(element);
	}

}
