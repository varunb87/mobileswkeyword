package com.algoworks.automationmobile.KeywordAction;

import java.time.Duration;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;
import org.testng.ITestContext;
import com.algoworks.automationmobile.Base.MainInitiator;
import com.algoworks.automationmobile.Base.StartDriver;
import com.algoworks.automationmobile.Reporting.ExtentReporting;
import com.algoworks.automationmobile.Utility.CreateMobileElement;
import com.algoworks.automationmobile.Utility.CustomWait;
import com.algoworks.automationmobile.Utility.TestNGParameters;
import com.algoworks.automationmobile.Utility.Utils;
import com.google.common.base.Throwables;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.ios.IOSDriver;

@SuppressWarnings("deprecation")
public class PerformAction
{
	private CreateMobileElement ce;
	protected AppiumDriver<MobileElement> driver = null;
	private MobileElement mobileElement = null;
	private ExtentReporting er = null;
	private TouchAction touch = null;
	private String failMsg = null;
	protected List<MobileElement> parentnode = null;
	private static Logger logger = LogManager.getLogger(PerformAction.class);
	private CustomWait wait;
	private TestNGParameters parameters;
	private static final String BLANK = "blank";

	public PerformAction(ITestContext testContext)
	{
		driver = StartDriver.getDriver();
		ce = new CreateMobileElement();
		er = MainInitiator.getExtentReportingObject();
		touch = new TouchAction(driver);
		wait = new CustomWait(driver);
		parameters = new TestNGParameters(testContext);
	}

	public void perform(String action, String element, int child, Boolean isList, String category, String value)
			throws InterruptedException
	{
		By locator;
		Boolean flag = false;
		String actual = null;
		int attempts = 0;
		String fieldLabel = null;
		element = element.trim();

		/* element cell CANNOT be blank here */
		if (Utils.isNotEmptyAndBlank(element) && !isList && child == -1 && !BLANK.equalsIgnoreCase(element))
		{
			locator = ce.getLocator(element, category);				/* For obtaining the element directly */
			wait.waitforElement(locator);
			mobileElement = driver.findElement(locator);
		}

		/* element cell CANNOT be blank here */
		else if (Utils.isNotEmptyAndBlank(element) && isList && child == -1 && !BLANK.equalsIgnoreCase(element))
		{
			locator = ce.getLocator(element, category);
			wait.waitforElements(locator, "small"); /* For those elements which have only 1 child */
			parentnode = driver.findElements(locator);
		}

		/* element cell CANNOT be blank here */
		else if (Utils.isNotEmptyAndBlank(element) && isList && child != -1 && !BLANK.equalsIgnoreCase(element))
		{
			locator = ce.getLocator(element, category);
			wait.waitforElements(locator, "large"); /* For those elements which have more than 1 child */
			parentnode = parentnode.get(child).findElements(locator);
		}

		/* element cell can be blank here */
		else if (Utils.isNotEmptyAndBlank(element) && !isList && child != -1 && parentnode != null)
		{
			mobileElement = parentnode.get(child);
		}
		Dimension signatureDimension;
		Dimension size;
		int startx;
		int starty;
		int endy;
		int endx;
		switch (action.toUpperCase())
		{
			case "ASSERT_DISABLED":
				fieldLabel = getFieldlabel(element);
				flag = mobileElement.isEnabled();

				if (flag)
				{
					logger.info(fieldLabel + " is disabled as expected.");
					er.passTest(fieldLabel + " is disabled as expected.");
				}

				else
				{
					failMsg = fieldLabel + " is enabled! It should have been disabled.";
					Assert.fail(failMsg);
				}
				break;

			case "ASSERT_ENABLED":
				fieldLabel = getFieldlabel(element);
				flag = mobileElement.isEnabled();

				if (flag)
				{
					logger.info(fieldLabel + " is enabled as expected.");
					er.passTest(fieldLabel + " is enabled as expected.");
				}

				else
				{
					failMsg = fieldLabel + " is disabled! It should have been enabled.";
					Assert.fail(failMsg);
				}
				break;

			case "CLEARBOX":
				mobileElement.clear();
				break;

			case "DRAW_SIG":
				signatureDimension = mobileElement.getSize();
				int oldX = signatureDimension.getWidth();
				int oldY = signatureDimension.getHeight();
				touch.press(oldX / 5, (int) (oldY / 1.2));
				touch.moveTo(oldX / 3, oldY / 2); /*
													 * OldX + X, OldY + Y
													 */
				touch.release();
				touch.perform();
				er.passTest("Signature was drawn successfully.");
				break;

			case "DO NOTHING":
				logger.info("No operation.");
				break;

			case "SCROLL_TEXT":
				if (driver instanceof AndroidDriver)
				{
					/*
					 * Scroll down the drop down until the Value = text to be searched is found
					 */
					AndroidDriver<MobileElement> androidDriver = (AndroidDriver<MobileElement>) driver;
					mobileElement = androidDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView("
							+ "new UiSelector().textContains(\"" + value + "\"));");

					// Now we tap the element found. The Value now becomes the
					// element.
					// mobileElement = (MobileElement)
					// driver.findElementByXPath("//android.widget.TextView[@text='"+value+"']");
					touch.tap(mobileElement);
					touch.perform();
					logger.info(value + " was found in the drop down & tapped successfully.");
					er.passTest(value + " was found in the drop down & tapped successfully.");
				}

				else if (driver instanceof IOSDriver)
				{
					IOSDriver<MobileElement> iosDriver = (IOSDriver<MobileElement>) driver;
					// element = element + "[\"" + value + "\"]";
					String script = element + ".scrollToVisible();";
					iosDriver.executeScript(script);
					// mobileElement = (MobileElement)
					// ios.findElementByIosUIAutomation(element);
					touch.tap(mobileElement);
					touch.perform();
					logger.info(value + " was found in the drop down & tapped successfully.");
					er.passTest(value + " was found in the drop down & tapped successfully.");
				}
				break;

			case "SWIPEDOWN":
				size = driver.manage().window().getSize();
				startx = size.getWidth() / 2;
				starty = (int) (size.getHeight() * 0.80);
				endy = (int) (size.getHeight() * 0.20);
				// driver.swipe(startx, starty, startx, endy, 1000);
				swipe(startx, starty, startx, endy, 1000);
				er.writeInfo("Page was scrolled down successfully.");
				break;

			case "SWIPEUP":
				size = driver.manage().window().getSize();
				startx = (size.getWidth() / 4);
				starty = (int) (size.getHeight() * 0.80);
				endy = (int) (size.getHeight() * 0.20);
				// driver.swipe(startx, endy, startx, endy, 2000);
				swipe(startx, endy, startx, endy, 2000);
				er.writeInfo("Page was scrolled up successfully.");
				break;

			case "SWIPE-LEFTTORIGHT":
				size = driver.manage().window().getSize();
				startx = (int) (size.width * 0.8);
				endx = (int) (size.width * 0.20);
				starty = size.height / 2;
				swipe(startx, starty, endx, starty, 1);
				logger.info("Page was scrolled left successfully.");
				er.writeInfo("Page was scrolled left successfully.");
				break;

			case "SWIPE-RIGHTTOLEFT":
				size = driver.manage().window().getSize();
				endx = (int) (size.width * 0.8);
				startx = (int) (size.width * 0.20);
				starty = size.height / 2;
				swipe(startx, starty, endx, starty, 1);
				logger.info("Page was scrolled right successfully.");
				er.writeInfo("Page was scrolled right successfully.");
				break;

			case "TAP":
				fieldLabel = getFieldlabel(element);
				mobileElement.click();
				er.passTest(fieldLabel + " was found & tapped successfully.");
				logger.info(fieldLabel + " was found & tapped successfully.");
				break;

			case "TAPBACK":
				if (driver instanceof AndroidDriver)
				{
					((AndroidDriver<MobileElement>) driver).pressKeyCode(AndroidKeyCode.BACK);
					logger.info("Back button of " + parameters.getDevicename() + " was tapped successfully.");
					er.passTest("Back button of " + parameters.getDevicename() + " was tapped successfully.");
				}
				break;

			case "TYPE":
				fieldLabel = getFieldlabel(element);
				typeInTextbox(value);
				er.passTest(value + " was typed in " + fieldLabel + " successfully.");
				logger.info(value + " was typed in " + fieldLabel + " successfully.");
				break;

			case "VANISH":
				Boolean isVisible = true;
				attempts = 0;
				while (isVisible && attempts <= 30) // as long as "Submitting.." is visible
				{
					try
					{
						if (mobileElement.isDisplayed())
						{
							er.writeInfo("Submitting data....");
						}
						else
						{
							isVisible = false;
						}
					}
					catch (NoSuchElementException | StaleElementReferenceException e)
					{
						isVisible = false;
					}
					++attempts;
				}
				break;

			case "VERIFY":
				actual = mobileElement.getText();
				flag = actual.equalsIgnoreCase(value);
				if (flag)
				{
					logger.info("Actual message matched with the expected. Message is " + actual);
					er.passTest("Actual message matched with the expected. Message is " + actual);
				}
				else if (actual.contains(value))
				{
					logger.info("Actual message matched with the expected. Message is " + actual);
					er.passTest("Actual message matched with the expected. Message is " + actual);
				}
				else
				{
					failMsg = "Actual message did not match the expected! Expected = " + value + " but Actual = "
							+ actual;
					Assert.fail(failMsg);
				}
				break;

			case "VERIFYMESSAGE":
				actual = mobileElement.getText();
				flag = actual.equalsIgnoreCase(value);
				if (flag)
				{
					logger.info("Actual message matched with the expected. Message is " + actual);
					er.passTest("Actual message matched with the expected. Message is " + actual);
				}
				else
				{
					failMsg = "Actual message did not match the expected!";
					Assert.fail(failMsg);
				}
				break;

			default:
				break;
		}
	}

	/**
	 * @param element
	 * @return
	 */
	private String getFieldlabel(String element)
	{
		String fieldLabel;
		try
		{
			fieldLabel = mobileElement.getText();
			if (fieldLabel.trim().isEmpty())
			{
				fieldLabel = element;
			} 
		}
		catch (NullPointerException e)
		{
			fieldLabel = element;
		}
		return fieldLabel;
	}

	private void swipe(int startx, int starty, int endx, int endy, int duration)
	{
		touch.press(startx, starty);
		touch.waitAction(Duration.ofSeconds(duration));
		touch.moveTo(endx, endy);
		touch.release();
		touch.perform();
	}

	private void typeInTextbox(String value)
	{
		if (driver instanceof IOSDriver)
		{
			mobileElement.setValue(value);
		}

		else if (driver instanceof AndroidDriver)
		{
			mobileElement.sendKeys(value);
		}

		try
		{
			driver.hideKeyboard();
		}
		catch (WebDriverException e)
		{
			String exception = Throwables.getStackTraceAsString(e);
			if (exception.contains("Soft keyboard not present") || exception.contains("Failed to hide keyboard"))
			{
				// do nothing
			}

			else
			{
				e.printStackTrace();
			}
		}
	}

	public String getFailMessage()
	{
		return failMsg;
	}
}
